{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        inherit (builtins) substring;
        pkgs = import nixpkgs { inherit system; };
      in
      {
        formatter = pkgs.nixpkgs-fmt;

        packages = rec {
          default = website;
          website = pkgs.stdenvNoCC.mkDerivation {
            pname = "acmelabs-website";
            version = let mtime = self.lastModifiedDate; in
              "${substring 0 4 mtime}-${substring 4 2 mtime}-${substring 6 2 mtime}";
            src = ./.;
            buildInputs = [ pkgs.zola ];
            buildPhase = ''
              zola build
            '';
            installPhase = ''
              mkdir -p $out
              cp -r public/* $out
            '';
          };
        };

        devShells.default = pkgs.mkShellNoCC {
          buildInputs = with pkgs; [ zola ];
        };
      });
}
