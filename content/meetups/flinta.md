+++
title = "FLINTA Treffen"

[extra]
schedule = "Jeden 1. und 3. Mittwoch im Monat ab 19Uhr"
+++

<div style="text-align: center;">
Jeden 1. und 3. Mittwoch im Monat ab 19Uhr
</div>

Offenes Treffen und Safespace für FLINTA-Menschen zur

* Vernetzung
* Wissensaustausch
* gemütlich zusammensitzen

Der [CoC der ACME Labs](@/coc.md) gilt auch für diese Veranstaltung.

An diesen Abenden sind die ACME Labs nur für Besuchende des FLINTA-Treffen geöffnet.

FLINTA steht für:
* Frauen
* Lesben
* intersex Menschen
* nicht-binäre Menschen
* trans Menschen
* agender Menschen
