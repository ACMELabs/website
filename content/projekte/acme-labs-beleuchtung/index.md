+++
title = "ACME Labs Beleuchtung"
description = "Eine LED-Decken-Beleuchtung für die Räumlichkeiten vom ACME Labs. Die Laternen sind 3D-gedruckt und lassen sich für die Farben ansteuern."
[extra]
responsible = "tauli"
status = "In Arbeit"
image = "acme-labs-beleuchtung.jpeg"
imageAlt = "A latern lit up in multiple colours hanging under the ceiling."
+++