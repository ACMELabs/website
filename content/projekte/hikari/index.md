+++
title = "Hikari"
description = "hikari ist ein stacking Window Manager mit zusätzlicher tiling Funktionalität und wurde stark vom Calm Window manager (cwm(1)) inspiriert."
[extra]
responsible = "raichoo"
url = "https://hikari.acmelabs.space"
status = "Abgeschlossen"
image = "hikari-desktop.png"
imageAlt = "The desktop of a computer. The layout has a minimalistic design."
+++
