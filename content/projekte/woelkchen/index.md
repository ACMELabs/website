+++
title = "Woelkchen"
description = "RGB LEDs an einem ESP mit Webserver, in einem Plastikgehäuse, dekoriert mit Watte, damit es wie eine Wolke aussieht."
[extra]
url = "https://github.com/Kaisa-Marysia/Woelkchen"
responsible = "Kascha"
status = "Done"
hint = "[GitHub Repo](https://github.com/Kaisa-Marysia/Woelkchen)"
image = "../default-images/project-soldering-iron.jpg"
imageAlt = "A soldering iron on a blue background."
+++