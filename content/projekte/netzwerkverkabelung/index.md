+++
title = "Netzwerkverkabelung im Lab"
description = "Anbringen des Netzwerkschranks und Auflegen der Netzwerkkabel auf Patchpanel."
[extra]
responsible = "bodems"
status = "In Progress"
image = "../default-images/project-soldering-iron.jpg"
imageAlt = "A soldering iron on a blue background."
+++