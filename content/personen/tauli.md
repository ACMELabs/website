+++
title = "tauli (er/ihm)"
[extra]
interests = "Programmieren (alles außer PHP), Sysadmin (alles außer Windows), ESP32 µC, 3D Druck, FreeCAD, Origami"
learning = "Rust, Gamedev"
emoji = "🍵"
+++
