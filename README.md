# website

Website for the hackspace ACMELabs.

### Setup and requirements

This project is build using the [Zola](https://www.getzola.org/) static page
generator (SPG), please follow the [installation instructions](https://www.getzola.org/documentation/getting-started/installation/).

#### Nix

The flake provided default devShell provides `zola` to you.

### Building

With `zola` installed on your maschine you can build this project with `zola
build`. The result will be placed in the `public/` folder.

#### Nix

This project can also be build using nix with `nix build .#website`.
