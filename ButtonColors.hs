{-
 - Zero dependency code for generating the css necessary for menu colors. Sould
 - run with "runghc ButtonColors.hs n" where n is a positive whole number.
 - Derived from old hakyll page code.
 -}

module Main where

import Data.Char (digitToInt, intToDigit, toUpper)
import System.Environment (getArgs)

main :: IO ()
main = do
  n :: Int <- read . head <$> getArgs
  putStrLn $ makeCss acmeColors n

acmeColors :: [String]
acmeColors = ["FF0000", "FF8D00", "ffdb00", "00d619", "009fff"]

makeCss :: [String] -> Int -> String
makeCss cs n = mconcat $ map toCss $ zip [1..] $ makeSpread cs n

toCss :: (Int,(String,String)) -> String
toCss (n,(c,b)) = mconcat
  [ "@include menu-button("
  , show n    -- number of the button
  , ", #", c  -- color of the button
  , ", #", b  -- color of the button border
  , ");\n"
  ]

makeSpread :: [String] -> Int -> [(String, String)]
makeSpread cs n =
  map (\c -> (intToHex c, intToHex $ calcBorderColor c))
  $ calcColors n
  $ map hexToInt cs

hexToInt :: String -> (Int, Int, Int)
hexToInt [rh, rl, gh, gl, bh, bl] = (r,g,b) where
  r = digitToInt rh * 16 + digitToInt rl
  g = digitToInt gh * 16 + digitToInt gl
  b = digitToInt bh * 16 + digitToInt bl
hexToInt _ = (0,0,0)

intToHex :: (Int, Int, Int) -> String
intToHex (r,g,b) = toUpper <$> [rh, rl, gh, gl, bh, bl] where
  rh = intToDigit (r `div` 16)
  rl = intToDigit (r `rem` 16)
  gh = intToDigit (g `div` 16)
  gl = intToDigit (g `rem` 16)
  bh = intToDigit (b `div` 16)
  bl = intToDigit (b `rem` 16)

calcColors :: Int -> [(Int,Int,Int)] -> [(Int,Int,Int)]
calcColors n []  = replicate n (0,0,0)
calcColors n [c] = replicate n c
calcColors n cs
  | n <= 0    = []
  | n == 1    = [head cs]
  | otherwise = map makeCol colorPos where
    nCol = toEnum $ length cs - 1
    n'   = toEnum n - 1
    colorPos = map (nCol / n' * ) [0..n']
    makeCol :: Float -> (Int, Int, Int)
    makeCol pos =
      let (rl, gl, bl) = cs !! floor pos
          (rh, gh, bh) = cs !! ceiling pos
          factor       = pos - toEnum (floor pos)
       in ( round ( toEnum rl * (1-factor) + toEnum rh * factor)
          , round ( toEnum gl * (1-factor) + toEnum gh * factor)
          , round ( toEnum bl * (1-factor) + toEnum bh * factor)
          )

calcBorderColor :: (Int, Int, Int) -> (Int, Int, Int)
calcBorderColor (r,g,b) = (r `div` 2, g `div` 2, b `div` 2)
